from django.urls import path
from django.contrib.auth.views import LoginView


from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('def_obj', views.definition_ojectif, name='def_obj'),
    path('investir_trader', views.investir_trader, name='investir_trader'),
]
