from django.shortcuts import render


def home(request):
    return render(request, 'home.html')


def definition_ojectif(request):
    return render(request, 'def_obj.html')


def investir_trader(request):
    return render(request, 'investir_trader.html')